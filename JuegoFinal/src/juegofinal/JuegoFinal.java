/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juegofinal;

import Models.ArmaFactory;
import Models.Jugador;
import Models.JugadorPacifico;
import Models.JugadorBueno;
import Models.JugadorMalo;
import Models.Mapa;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

/**
 *
 * @author Drugdu
 */
public class JuegoFinal {

    static Mapa m;
    static Jugador j1;
    static Jugador j2;
    static Jugador j3;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        InputStreamReader br = new InputStreamReader(System.in);
        Random R= new Random();
        j1= new JugadorBueno();
        j1.EquiparArma(ArmaFactory.CreateArma(R.nextInt(10)));
        j1.setChar('1');
        j2= new JugadorPacifico();
        j2.EquiparArma(ArmaFactory.CreateArma(R.nextInt(10)));
        j2.setChar('2');
        j3= new JugadorMalo();
        j3.EquiparArma(ArmaFactory.CreateArma(R.nextInt(10)));
        j3.setChar('3');
        CrearMapa();
        while(j2.isVivo()&&j1.isVivo()&&j3.isVivo())
        {
            j1.ImprimirJugador();
            j2.ImprimirJugador();
            j3.ImprimirJugador();
            m.ImprimirMapa();

            System.out.println("");
            j1.Jugar(m,j2);
            System.out.println("");
            j2.Jugar(m,j1);
            System.out.println("");
            j3.Jugar(m, j1);
            System.out.println("");
            
            
        }
        
    }

    private static void CrearMapa() {
        m= new Mapa(); 
        m.ubicarJugador(1,1,j1);
        m.ubicarJugador(48,48,j2);
        m.ubicarJugador(48,1, j3);
        
    }
    
}
